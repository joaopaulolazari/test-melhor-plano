import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Broadband from '@/components/Broadband';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/broadbands',
            name: 'Broadbands',
            component: Broadband
        },
    ]
});
