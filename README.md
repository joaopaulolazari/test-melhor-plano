# Requiriments #

* [NPM](https://docs.npmjs.com/getting-started/installing-node)
* [COMPOSER](https://getcomposer.org/)
* [PHP7](https://php.net/)

# How to install #
### API ###

```sh
$ cd path_to_api
$ composer install
$ php -S localhost:8000
```

### FRONT ###

```sh
$ cd path_to_front
$ npm install
$ npm run start
```

# Explicação das tecnologias utilizadas
### API
* [SLIM Framework](https://docs.slimframework.com/)

EscolhiEscolhi esse framework por ser uma stack bem simples para uma funcionalidade simples como a pedida, é um framework focado para do desenvolvimento de Api RestFul.
### FRONT
* [Vue](https://vuejs.org/)
* [Vuetify](https://vuetifyjs.com/en/)

EscolhiEscolhi o Vue por ser um framework em crescimento para javascript, para mostrar uma desenvoltura com uma nova tecnologia e também por achar algo importante.
E escolhi o Vuetify para melhorar o layout.