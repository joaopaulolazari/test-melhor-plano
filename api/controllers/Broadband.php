<?php
namespace controllers {
    /**
     * Class Broadband
     * @package controllers
     */
    class Broadband {
        // database
        private $database;

        /**
         * Broadband constructor.
         */
        public function __construct()
        {
            $str = file_get_contents('./config/database.json');
            $this->database = json_decode($str, true);
        }

        /**
         * GetAll database
         */
        public function getAll()
        {
            global $app;
            $app->render('default.php',
                ["data" => $this->database],
                200
            );
        }

        /**
         * @param $id
         * @return array
         */
        public function getAllConnectionsForService($id)
        {
            $array = [];
            foreach ($this->database['connections'] as $connection) {
                if ($connection['idFrom'] === $id) {
                    $array[] = [
                        'from' => $this->getService($connection['idFrom']),
                        'to' => $this->getService($connection['idTo']),
                        'price' => $connection['price'],
                    ];
                }
            }
            if (! empty($array)) {
                return $array;
            } else {
                return null;
            }
        }

        /**
         * @param $id
         * @return array
         */
        public function getService($id)
        {
            foreach ($this->database['services'] as $service) {
                if ($service['id'] === $id) {
                    return [
                        'name' => $service['name'],
                        'price' => $service['price'],
                    ];
                }
            }
            return [];
        }

        /**
         * GetAllBroadband get all broadbands and nodes
         */
        public function getAllBroadband()
        {
            global $app;

            $result = [];
            $i = 0;
            foreach ($this->database['services'] as $service) {
                if ($service['type'] === 'bb') {
                    $result[$i] = $service;
                    $result[$i]['connections'] = [];
                    $j = 0;
                    foreach ($this->database['connections'] as $connection) {
                        if ($connection['idFrom'] === $service['id']) {
                            $result[$i]['connections'][$j] = [
                                'from' => $this->getService($connection['idFrom']),
                                'to' => $this->getService($connection['idTo']),
                                'price' => $connection['price'],
                            ];
                            if (! is_null($this->getAllConnectionsForService($connection['idTo']))) {
                                $result[$i]['connections'][$j]['forward'] = $this->getAllConnectionsForService($connection['idTo']);
                            }
                            $j++;
                        }
                    }
                }
                $i++;
            }

            $app->render('default.php',
                ["data" => $result],
                200
            );
        }
    }
}
