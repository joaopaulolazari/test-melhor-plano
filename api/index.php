<?php
//Autoload
use controllers\Broadband;

$loader = require 'vendor/autoload.php';

//Instanciando objeto
$app = new \Slim\Slim(array(
    'templates.path' => 'templates'
));

// CORS
$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);
$app->add($cors);

//Listando todas
$app->get('/list-all-broadband', function() use ($app){
    (new Broadband($app))->getAllBroadband();
});

//Rodando aplicação
$app->run();
